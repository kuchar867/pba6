package com.example.demo.app;

import com.example.demo.exception.ErrorFactory;
import com.example.demo.model.CreateRequest;
import com.example.demo.service.JwsService;
import com.example.demo.utils.JwtUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.digest.HmacAlgorithms;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ReadListener;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * @author jakub
 * 14.05.2023
 */
@Component
@Order(1)
public class HttpFilter extends OncePerRequestFilter {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private JwsService jwsService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        CachedRequestHttpServletRequest cachedRequest = new CachedRequestHttpServletRequest(request);

        if (cachedRequest.getMethod().equals("POST") && !cachedRequest.getRequestURL().toString().contains("/jws")) {
            if (validXHmacSign(response, cachedRequest)) return;
        }

        if (cachedRequest.getMethod().equals("PUT") && !cachedRequest.getRequestURL().toString().contains("/jws")) {
            if (validJwsToken(response, cachedRequest)) return;
        }

        filterChain.doFilter(cachedRequest, response);
    }

    private boolean validJwsToken(HttpServletResponse response, CachedRequestHttpServletRequest cachedRequest) throws IOException {
        String testedRequest = IOUtils.toString(cachedRequest.getInputStream());

        String token = jwsService.generateToken(objectMapper.readValue(testedRequest, CreateRequest.class));

        if (!token.equals(cachedRequest.getHeader("X-JWS-SIGNATURE"))) {
            handleInvalidSignature(response);
            return true;
        }

        return false;
    }

    private boolean validXHmacSign(HttpServletResponse response, CachedRequestHttpServletRequest cachedRequest) throws IOException {
        HmacUtils hm1 = new HmacUtils(HmacAlgorithms.HMAC_SHA_256, "123456".getBytes());

        String testedRequest = IOUtils.toString(cachedRequest.getInputStream());

        final String signature = hm1.hmacHex(testedRequest);

        if (!signature.equals(cachedRequest.getHeader("X-HMAC-SIGNATURE"))) {
            handleInvalidSignature(response);
            return true;
        }
        return false;
    }

    private void handleInvalidSignature(HttpServletResponse response) throws IOException {
        var error = ErrorFactory.createError(HttpStatus.UNPROCESSABLE_ENTITY);

        response.setStatus(HttpStatus.UNPROCESSABLE_ENTITY.value());
        response.getWriter().write(objectMapper.writeValueAsString(error));
    }

    private static class CachedRequestHttpServletRequest extends HttpServletRequestWrapper {

        private final byte[] cachedBody;

        public CachedRequestHttpServletRequest(HttpServletRequest request) throws IOException {
            super(request);
            this.cachedBody = StreamUtils.copyToByteArray(request.getInputStream());
        }

        @Override
        public ServletInputStream getInputStream() {
            ByteArrayInputStream inputStream = new ByteArrayInputStream(this.cachedBody);

            return new ServletInputStream() {
                @Override
                public boolean isFinished() {
                    return inputStream.available() == 0;
                }

                @Override
                public boolean isReady() {
                    return true;
                }

                @Override
                public int read() {
                    return inputStream.read();
                }

                @Override
                public void setReadListener(ReadListener readListener) {
                    throw new UnsupportedOperationException();
                }
            };
        }
    }

    // other methods
}
