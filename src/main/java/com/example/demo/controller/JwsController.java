package com.example.demo.controller;

import com.example.demo.model.CreateRequest;
import com.example.demo.service.JwsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jakub
 * 14.05.2023
 */
@RestController
@RequestMapping(path = "/api/v1/jws")
public class JwsController {

    @Autowired
    private JwsService jwsService;

    @PostMapping
    public ResponseEntity<String> getJwsForJson(@RequestBody CreateRequest body) {
        return ResponseEntity.ok().body(jwsService.generateToken(body));
    }
}
