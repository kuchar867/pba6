package com.example.demo.utils;

import org.springframework.beans.BeanUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * @author jakub
 * 15.04.2023
 */
public class ObjectMapper<Entity, Dto> {
    public List<Dto> mapToDtoList(List<Entity> entities, Class<Dto> dtoClass) {
        return entities.stream().map(x -> mapToDto(x, dtoClass)).collect(Collectors.toList());
    }

    public List<Entity> mapToEntityList(List<Dto> entities, Class<Entity> entityClass) {
        return entities.stream().map(x -> mapToEntity(x, entityClass)).collect(Collectors.toList());
    }

    public Dto mapToDto(Entity entity, Class<Dto> dtoClass) {
        try {
            Dto dto = dtoClass.newInstance();
            PropertyDescriptor[] entityPds = BeanUtils.getPropertyDescriptors(entity.getClass());
            PropertyDescriptor[] dtoPds = BeanUtils.getPropertyDescriptors(dtoClass);

            for (PropertyDescriptor dtoPd : dtoPds) {
                Method dtoReadMethod = dtoPd.getReadMethod();
                Method dtoWriteMethod = dtoPd.getWriteMethod();
                if (dtoReadMethod != null && dtoWriteMethod != null) {
                    PropertyDescriptor entityPd = getPropertyDescriptorByName(entityPds, dtoPd.getName());
                    if (entityPd != null) {
                        Method entityReadMethod = entityPd.getReadMethod();
                        if (entityReadMethod != null) {
                            Object value = entityReadMethod.invoke(entity);
                            if (entityPd.getPropertyType().equals(String.class) && entityPd.getName().equals("id")) {
                                value = UUID.fromString((String) value);
                            }
                            dtoWriteMethod.invoke(dto, value);
                        }
                    }
                }
            }
            return dto;
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("Failed to map entity to DTO", e);
        }
    }

    public Entity mapToEntity(Dto dto, Class<Entity> entityClass) {
        try {
            Entity entity = entityClass.newInstance();
            PropertyDescriptor[] entityPds = BeanUtils.getPropertyDescriptors(entityClass);
            PropertyDescriptor[] dtoPds = BeanUtils.getPropertyDescriptors(dto.getClass());

            for (PropertyDescriptor entityPd : entityPds) {
                Method entityReadMethod = entityPd.getReadMethod();
                Method entityWriteMethod = entityPd.getWriteMethod();
                if (entityReadMethod != null && entityWriteMethod != null) {
                    PropertyDescriptor dtoPd = getPropertyDescriptorByName(dtoPds, entityPd.getName());
                    if (dtoPd != null) {
                        Method dtoReadMethod = dtoPd.getReadMethod();
                        if (dtoReadMethod != null) {
                            Object value = dtoReadMethod.invoke(dto);
                            if (value instanceof UUID && dtoPd.getPropertyType().equals(String.class)) {
                                value = value.toString();
                            }
                            entityWriteMethod.invoke(entity, value);
                        }
                    }
                }
            }
            return entity;
        } catch (ReflectiveOperationException e) {
            throw new RuntimeException("Failed to map DTO to entity", e);
        }
    }

    private PropertyDescriptor getPropertyDescriptorByName(PropertyDescriptor[] propertyDescriptors, String propertyName) {
        for (PropertyDescriptor pd : propertyDescriptors) {
            if (pd.getName().equals(propertyName)) {
                return pd;
            }
        }
        return null;
    }
}
