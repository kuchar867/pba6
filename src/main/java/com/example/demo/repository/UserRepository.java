package com.example.demo.repository;

import com.example.demo.entity.UserEntity;
import org.springframework.stereotype.Repository;

/**
 * @author jakub
 * 15.04.2023
 */
@Repository
public interface UserRepository extends BaseRepository<UserEntity, String> {
}
