package com.example.demo.service;

import com.example.demo.exception.UnprocesableEntityException;
import com.example.demo.model.CreateRequest;
import com.example.demo.utils.JwtUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jakub
 * 14.05.2023
 */
@Service
public class JwsService {
    @Autowired
    private JwtUtils jwtUtils;

    @Autowired
    private ObjectMapper objectMapper;

    public String generateToken(CreateRequest createRequest) {
        try {
            return jwtUtils.generateToken(objectMapper.writeValueAsString(createRequest));
        } catch (JsonProcessingException e) {
            throw new UnprocesableEntityException();
        }
    }
}
